package message

//MenuFTP retorna um menu de seleção para interação client->servidorFTP
func MenuFTP() (string, int8) {
	return "Escolha uma opção:\n" +
		"1 - Realizar Download\n" +
		"2 - Realizar Upload\n" +
		"3 - Sair\nOpção: ", 3
}
