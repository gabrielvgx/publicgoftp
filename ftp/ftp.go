package ftp

import (
	"errors"
	"fmt"
	"os"

	"github.com/secsy/goftp"
)

//ConnectionDefault realiza uma conexão default com servidor localhost
func ConnectionDefault() (*goftp.Client, error) {
	config := goftp.Config{
		User:     "cantalk",
		Password: "cantalk",
	}
	client, err := goftp.DialConfig(config, "localhost")
	return client, err
}

//Download realiza a transmissão de arquivos do servidor FTP para o cliente
func Download(origin, dest string, connectionClient *goftp.Client) (*os.File, error) {
	file, err := os.Create(dest)
	if err != nil {
		fmt.Println(err, "Falha ao acessar diretório para salvar arquivo")
	}
	fmt.Println("Create(", dest, ") -> ", "Retrieve(", origin, ",", file.Name, ")")
	err = connectionClient.Retrieve(origin, file)
	if err != nil {
		return nil, errors.New("Erro ao realizar Download do arquivo")
	}
	return file, nil
}

//Upload realizar transmissão de arquivos do cliente para o servidor FTP
func Upload(origin, dest string, connectionClient *goftp.Client) error {
	file, err := os.Open(origin)
	if err != nil {
		fmt.Println(err, "Erro ao ler arquivo local")
	}
	err = connectionClient.Store(dest, file)
	if err != nil {
		return errors.New("Erro ao realizar Upload de arquivo")
	}
	return nil
}
