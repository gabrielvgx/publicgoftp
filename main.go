package main

import (
	"app-ftp/ftp"
	"app-ftp/message"
	"fmt"
)

func main() {
	client, _ := ftp.ConnectionDefault()
	menu, exitOp := message.MenuFTP()
	var op int8
	for op != exitOp {
		fmt.Print(menu)
		fmt.Scan(&op)
		switch op {
		case 1:
			fmt.Println("Informe o nome do arquivo que deseja realizar Download")
			var nameFile, nameSaveFile string
			fmt.Scan(&nameFile)
			fmt.Println("Informe o diretório para onde deseja salvar o arquivo")
			fmt.Scan(&nameSaveFile)
			_, err := ftp.Download(nameFile, nameSaveFile, client)
			if err != nil {
				fmt.Println(err)
				continue
			}
			fmt.Println("Download realizado com sucesso!")
		case 2:
			fmt.Println("Informe o diretório e nome do arquivo que deseja realizar Upload")
			var nameFile, nameSaveFile string
			fmt.Scan(&nameFile)
			fmt.Println("Informe o nome como deseja salvar este arquivo")
			fmt.Scan(&nameSaveFile)
			err := ftp.Upload(nameFile, nameSaveFile, client)
			if err != nil {
				fmt.Println(err)
				continue
			}
			fmt.Println("Upload realizado com sucesso!")
		case 3:
		default:
			fmt.Println("Opção Inválida! Tente novamente!")
		}
	}
}
